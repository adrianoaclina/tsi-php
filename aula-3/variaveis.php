<?php
echo "<pre>";
// 
define('QTD_PAGINAS', 10);

echo "Valor da minha constante é: " . QTD_PAGINAS;

// variavel para passar valor para uma constante
$ip_banco = '192.168.45.12';

define('IP_BANCO', $ip_banco);

echo "\nO IP do SGDB é: " . IP_BANCO;

// CONSTANTES MÁGICAS
echo "\nEstou na linha: " . __LINE__;
echo "\nEste é o arquivo: " . __FILE__;



// Muito bom para depurar o código
echo "\n\n";
var_dump($ip_banco);

$dias = ['dom',
         'seg',
         'ter',
         'qua',
         'qui',
         'sex',
         'sab',
         ];
unset($dias);// destroi a variavel

$dias[0] = 'dom';
$dias[1] = 'seg';
$dias[2] = 'ter';
$dias[3] = 'qua';
$dias[4] = 'qui';
$dias[5] = 'sex';
$dias[6] = 'sab';

unset($dias);

$dias = array(0 => 'dom',
              1 => 'seg',
              2 => 'ter',
              3 => 'qua',
              4 => 'qui',
              5 => 'sex',
              6 => 'sab',);
var_dump($dias);
echo "</pre>";