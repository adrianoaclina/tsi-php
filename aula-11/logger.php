<?php

// function logger(string $str) : bool{
//     $fp = fopen('log.txt', 'a');
//     if (fwrite($fp, $str)) {
//         fclose($fp);
//         return false;
//     } else {
        
//         fclose($fp);
//         return true;
//     }
    

// }
/* Exemplo com mais de um parâmetro */
function logger(string $str, int $nr_linha = null) : bool{
    $fp = fopen('log.txt', 'a');
    if (fwrite($fp, $nr_linha . ': '. $str)) {
        fclose($fp);
        return false;
    } else {
        
        fclose($fp);
        return true;
    }
    

}