<?php

$alunos = array(0 => array('nome' => 'Luiz Fer...',
                            'bitbucket' => 'https://bitbuck..'),
                1 => array('nome' => 'Adriano',
                            'bitbucket' => 'https://bitbuck..'),
                2 => array('nome' => 'Carlos',
                            'bitbucket' => 'https://bitbuck..'),
                3 => array('nome' => 'Dezena',
                            'bitbucket' => 'https://bitbuck..'),
                4 => array('nome' => 'Vinicius',
                            'bitbucket' => 'https://bitbuck..'),
                5  => array('nome' => 'José',
                            'bitbucket' => 'https://bitbuck..'));

// -- FOR 
// for ($cont = 0; $cont < count($alunos); $cont++) { 
//     echo $alunos[$cont]['nome'];
//     echo "<br>";
//     echo $alunos[$cont]['bitbucket'];
//     echo "<br>";    
// }


// -- FOREACH
echo '<table border="1">
        <thead>
        <th>
            Nome
        </th>
        <th>
            BitBucket
        </th>
        </thead>';
foreach ($alunos as $key => $value) {
    // $cor = 'yellow';
    $cor = $key % 2 == 0 ? 'yellow' : 'red';
    echo "<tr style='background-color: $cor'>
            <td>
                {$alunos[$key]['nome']}
            </td>
            <td>
                {$alunos[$key]['bitbucket']}
            </td>";
    
}
echo '</table>';
echo "<br/>";



echo '<table border="1">
        <thead>
        <th>
            Nome
        </th>
        <th>
            BitBucket
        </th>
        </thead>';
foreach ($alunos as $key => $value) {
    // $cor = 'yellow';
    if($key % 2 == 0){
        $cor = 'blue';
    } else{
        $cor = 'green';
    }
    echo "<tr style='background-color: $cor'>
            <td>
                {$alunos[$key]['nome']}
            </td>
            <td>
                {$alunos[$key]['bitbucket']}
            </td>";
    
}
echo '</table>';

$nome = 'Adriano Aclina';

$posicao = strpos($nome, 'cli');

if($posicao !== false){
    echo "Encontrado na posição $posicao!!";
}

echo '<br><br>';