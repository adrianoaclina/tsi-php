<?php

if ($db = mysqli_connect(
    'localhost',
    'root',
    '',
    'aula_php',
    3307
)) {

} else {
    die("Problema ao conectar ao SGDB");
}
$login = null;

if (isset($_POST['email']) && ($_POST['password'])) {
    $p = mysqli_prepare($db, 'SELECT nome, senha FROM tb_usuario WHERE email = ?');
    mysqli_stmt_bind_param($p, 's', $_POST['email']);
    mysqli_stmt_execute($p);
    $result = mysqli_stmt_get_result($p);
    // $existe = $result->num_rows;
    $usuario = $result->fetch_assoc();

    $verifyPass = password_verify($_POST['password'], $usuario['senha']);
    if($verifyPass){
        $login = true;
    } else {
        $login = false;
    }
}
if($login === true){
    session_start();

    $_SESSION['id_usuario'] = $usuario['nome'];
    $_SESSION['nome_usuario'] = $usuario['nome'];
    
    include('menu.php');

    exit();
}

if ($login === false) {
    
    echo '<br>Login ou senha inválida<br>';
}

include('views/login.view.php');